var http = require("http");

http.createServer(function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    var x = 2;
    var y = 3;
    var z = 4;
    response.end('Calculo da expressao aritmetica (2+3*4): ' + eval(x + y * z));
}).listen(8081);

console.log('Server running at http://127.0.0.1:8081/');

