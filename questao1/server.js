function reverseString(str) {
    var strArray = str.split("");
    strArray.reverse();
    var strReverse = strArray.join("");
    return strReverse;
  }
  
   var http = require("http");
   
http.createServer(function (request, response) {
   response.writeHead(200, {'Content-Type': 'text/plain'});
   response.end('String: amor \n' + 'String invertida: ' + reverseString("amor\n"));
}).listen(8081);

console.log('Server running at http://127.0.0.1:8081/');

/*

const http = require('http')
const port = 8081
const ip = 'localhost'

const server = http.createServer((req, res) => {
    console.log('Recebendo uma request!')
    res.end('<h1>Aqui fica o que vamos enviar para o navegador como resposta!</h1>')
})

function reverseString(str) {
    var strArray = str.split("");
    strArray.reverse();
    var strReverse = strArray.join("");
    return strReverse;
  }

server.listen(port, ip, () => {
    console.log(`Servidor rodando em http://${ip}:${port}`)
    console.log('Para derrubar o servidor: ctrl + c');
    var readline = require('readline');
    var resp = "";

    var leitor = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    leitor.question("Informe uma String: ", function (answer) {
        var resp = answer;
        console.log("String invertida: " + reverseString(resp));
        leitor.close();
    });
})


*/