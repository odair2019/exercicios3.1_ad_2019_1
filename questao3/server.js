var PORT = 8081;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var server = dgram.createSocket('udp4');

server.on('listening', function () {
    var address = server.address();
    var sentence = 'odair xavier da silva.';

    console.log('UDP Server listening on ' + address.address + ":" + address.port);
    console.log('\nMinusculo: ' + sentence);
    console.log('\nMaiusculo: ' + sentence.toUpperCase());
    console.log('\nPara derrubar o servidor: ctrl + c');
});

server.on('message', function (message, remote) {
    console.log(remote.address + ':' + remote.port + ' - ' + message);

});

server.bind(PORT, HOST);
