const http = require('http')
const port = 8081
const ip = 'localhost'

const server = http.createServer((req, res) => {
    console.log('Recebendo uma request!')
    res.end('<h1>Aqui fica o que vamos enviar para o navegador como resposta!</h1>')
})

function reverseString(str) {
    var strArray = str.split("");
    strArray.reverse();
    var strReverse = strArray.join("");
    return strReverse;
}

function stringToHex(str) {
    let bufStr = Buffer.from(str, 'utf8');
    return bufStr.toString('hex');
}

server.listen(port, ip, () => {
    console.log(`Servidor rodando em http://${ip}:${port}`)
    console.log('Para derrubar o servidor: ctrl + c');
    var readline = require('readline');
    var resp = "";

    var leitor = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    leitor.question("Informe uma String: ", function (answer) {
        var resp = answer;
        console.log("String em hexadecimal: " + stringToHex(resp));
        leitor.close();
    });
})

